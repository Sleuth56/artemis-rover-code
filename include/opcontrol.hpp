#include "main.h"
#pragma once

extern Controller master;

extern Motor frontLeftMotor;
extern Motor backLeftMotor;
extern Motor frontRightMotor;
extern Motor backRightMotor;
extern std::shared_ptr<OdomChassisController> chassis;

extern pros::Imu IMU;