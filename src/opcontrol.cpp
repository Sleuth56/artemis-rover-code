#include "main.h"
using std::string;
using std::to_string;

// Controller declaration
Controller master (ControllerId::master);

// Motor configs
Motor frontLeftMotor(13, false, AbstractMotor::gearset::green, AbstractMotor::encoderUnits::degrees);
Motor backLeftMotor(15, false, AbstractMotor::gearset::green, AbstractMotor::encoderUnits::degrees);
Motor frontRightMotor(16, true, AbstractMotor::gearset::green, AbstractMotor::encoderUnits::degrees);
Motor backRightMotor(17, true, AbstractMotor::gearset::green, AbstractMotor::encoderUnits::degrees);

// 3-Axis accelerometer and gyro
pros::Imu IMU(14);

// Ultrasonics
pros::ADIUltrasonic RightUltra('G', 'H');
pros::ADIUltrasonic LeftUltra('A', 'B');
pros::ADIUltrasonic FrontUltra ('E', 'F');

//Control veriables
bool isAuto = false;
int adjustment = 0;
int Mode = 0;
double driveSpeed = 0.2;

// Global ultrasonic values
int LeftUltraValue;
int RightUltraValue;
int FrontUltraValue;

double difference=((LeftUltraValue-RightUltraValue));

// Chassis Configs
std::shared_ptr<OdomChassisController> chassis = ChassisControllerBuilder()
	.withMotors(frontLeftMotor, frontRightMotor, backRightMotor, backLeftMotor)
  .withMaxVelocity(70)
    .withGains(
        {0.0025, 0, 0}, // Distance controller gains
        {0.002, 0, 0}, // Turn controller gains
        {0.002, 0, 0.00006}  // Angle controller gains (helps drive straight)
    )
    .withDimensions(AbstractMotor::gearset::green, {{4_in, 8.5_in}, imev5GreenTPR})
    .withOdometry() // use the same scales as the chassis (above)
    .buildOdometry(); // build an odometry chassis

std::shared_ptr<okapi::SkidSteerModel> driveTrain = std::dynamic_pointer_cast<SkidSteerModel>(chassis->getModel());

// User controls
void Controls(void* param) {
  int loopCount = 0;
  while (true) {
    if (master.getDigital(ControllerDigital::X)) {
      chassis->stop();
      Mode = 1;
    }
    else if (master.getDigital(ControllerDigital::A)) {
      Mode = 2;
    }
    else if (master.getDigital(ControllerDigital::B)) {
      Mode = 3;
    }
    else if (master.getDigital(ControllerDigital::Y)) {
      Mode = 4;
    }

    if (master.getDigital(ControllerDigital::R1) && driveSpeed < 1) {
        driveSpeed += 0.1;
        pros::delay(200);
    }
    else if (master.getDigital(ControllerDigital::R2) && driveSpeed > 0) {
      driveSpeed -= 0.1;
      pros::delay(200);
    }

    // Controller readouts
    if (loopCount >= 100) {
      master.setText(1, 0, "Adj:" + to_string(adjustment) + "  " + to_string(driveSpeed)  + "           ");
      loopCount = 0;
    }
    loopCount += 1;
    pros::delay(20);
  }
}
pros::Task Controls_TR(Controls, (void*)"PROS", TASK_PRIORITY_DEFAULT, TASK_STACK_DEPTH_DEFAULT, "Controls Task");


// Main auto loop
void opcontrol() {
  IMU.reset();
  pros::delay(2000);

  while (true) {
    // Graphs the drive motor temps
    lv_chart_set_next(chart, NavyLine, frontLeftMotor.getTemperature());
    lv_chart_set_next(chart, BlueLine, backLeftMotor.getTemperature());
    lv_chart_set_next(chart, GreenLine, frontRightMotor.getTemperature());
    lv_chart_set_next(chart, LimeLine, backRightMotor.getTemperature());

    // Sensor debug information
    updateLineVariable(1, isAuto);
    updateLineVariable(2, FrontUltraValue);
    updateLineVariable(3, LeftUltraValue);
    updateLineVariable(4, RightUltraValue);
    updateLineVariable(5, difference);

    // This pulls ultrasonic values one time per cycle. (Helps with interfearence and consistancy for math and logic functions)
    LeftUltraValue = LeftUltra.get_value();
    RightUltraValue = RightUltra.get_value();
    FrontUltraValue = FrontUltra.get_value();

    // Update control veriables
    difference=((LeftUltraValue-RightUltraValue) - adjustment);

    if (master.getDigital(ControllerDigital::L2)) {
      chassis->stop();
    }
    // RC drive
    else if (Mode == 0) {
      chassis->getModel()->arcade((master.getAnalog(ControllerAnalog::leftY)/2), (master.getAnalog(ControllerAnalog::rightX)/2), 0.03);
    }
    // Macro
    else if (Mode == 1) {
      if (master.getAnalog(okapi::ControllerAnalog::leftY) > 0.3) {
        chassis->moveDistance(4_in);
      }
      else if (master.getAnalog(okapi::ControllerAnalog::leftY) < -0.3) {
        chassis->moveDistance(-4_in);
      }
      else if (master.getAnalog(okapi::ControllerAnalog::rightX) > 0.3) {
        chassis->turnAngle(30_deg);
      }
      else if (master.getAnalog(okapi::ControllerAnalog::rightX) < -0.3) {
        chassis->turnAngle(-30_deg);
      }
    }
    // Drive in the center of the tunnel
    else if (Mode == 2) {
      if (master.getDigital(ControllerDigital::left)) {
        adjustment -= 100;
      }
      else if (master.getDigital(ControllerDigital::right)) {
        adjustment += 100;
      }
      difference /= 2000;
      if (difference > 0.3) {
        difference = 0.3;
      }
      else if (difference < -0.3) {
        difference = -0.3;
      }
      chassis->getModel()->arcade(driveSpeed, -(difference));
    }
    // Just Drive Strate
    else if (Mode == 3) {
      if (master.getDigital(ControllerDigital::up)) {
        chassis->getModel()->arcade(driveSpeed, 0);
      }
      else if (master.getDigital(ControllerDigital::down)) {
        chassis->getModel()->arcade(-driveSpeed, 0);
      }
      else {
        chassis->stop();
      }
    }
    // Bumper Car Mode
    else if (Mode == 4) {
      if (FrontUltraValue > 50) {
        chassis->getModel()->arcade(driveSpeed, 0);
      }
      else {
        chassis->moveDistance(-3_in);
        chassis->turnAngle(20_deg);
      }
    }
    pros::delay(20);
  }
}